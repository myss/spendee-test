How to run

1. Run: `docker-compose up --build`
2. Wait couple seconds for image to build and containers spool up, when ready you should see something like this:
`php_1    | [02-Sep-2019 06:37:16] NOTICE: ready to handle connections`
3. You can access API like this: `http://localhost:8080/watch/1`

For more details about how to access certain services, see `docker-compose.yml`