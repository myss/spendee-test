<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190901235738 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO watch (title, price, description, created_at, updated_at) VALUES (\'Rolex\', 200, \'Lorem rolex ipsum\', \'2019-09-02 01:58:35\', \'2019-09-02 01:58:35\')');
        $this->addSql('INSERT INTO watch (title, price, description, created_at, updated_at) VALUES (\'Swatch\', 350, \'Lorem swatch ipsum\', \'2019-09-02 01:58:35\', \'2019-09-02 01:58:35\')');
        $this->addSql('INSERT INTO watch (title, price, description, created_at, updated_at) VALUES (\'Casio\', 10, \'Lorem casio ipsum\', \'2019-09-02 01:58:35\', \'2019-09-02 01:58:35\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE watch');
    }
}
