<?php

namespace App\Controller;

use App\Repository\Exception\MySqlWatchNotFoundException;
use App\Response\JsonError;
use App\Response\JsonSuccess;
use App\Service\Watch\Exception\WatchIdNotValidException;
use App\Service\Watch\WatchService;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WatchController extends AbstractController
{
    private $watchService;

    public function __construct(WatchService $watchService)
    {
        $this->watchService = $watchService;
    }

    /**
     * @Route("/watch/{id}", name="watch_by_id")
     */
    public function getByIdAction($id): Response
    {
        try {
            $result = $this->watchService->getWatchById($id);
        } catch (RuntimeException $exception) {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;

            if ($exception instanceof WatchIdNotValidException) {
                $code = Response::HTTP_BAD_REQUEST;
            } elseif ($exception instanceof MySqlWatchNotFoundException) {
                $code = Response::HTTP_NOT_FOUND;
            }

            return new JsonError($exception, $code);
        }

        return new JsonSuccess($result);
    }
}
