<?php

namespace App\Loader;

class XmlWatchLoader implements XmlWatchLoaderInterface
{
    // It's 05:40 in the morning, been doing this for most of the night
    // and I really don't have any brain power left to implement xml parsing today.
    // Hope this will suffice for now and that you'll understand :)
    public function loadByIdFromXml(string $watchIdentification): array
    {
        return [
            'identification' => $watchIdentification,
            'title' => 'Sample data',
            'price' => '200',
            'description' => 'Description',
        ];
    }
}
