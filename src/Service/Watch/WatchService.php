<?php

namespace App\Service\Watch;

use App\Loader\XmlWatchLoaderInterface;
use App\Repository\MySqlWatchRepositoryInterface;
use App\Service\Watch\Enum\DataSourceEnum;
use App\Service\Watch\Enum\SlowDataSourceEnum;
use App\Service\Watch\Exception\SlowDataSourceNotValidException;
use App\Service\Watch\Exception\WatchIdNotValidException;
use DateInterval;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class WatchService
{
    private const CACHE_PREFIX = 'watch';

    private $cacheAdapter;
    private $mySqlWatchRepository;
    private $xmlWatchLoader;
    private $slowDataSource;

    public function __construct(
        MySqlWatchRepositoryInterface $mySqlWatchRepository,
        XmlWatchLoaderInterface $xmlWatchLoader,
        AdapterInterface $cacheAdapter,
        string $slowDataSource
    )
    {
        $this->mySqlWatchRepository = $mySqlWatchRepository;
        $this->xmlWatchLoader = $xmlWatchLoader;
        $this->cacheAdapter = $cacheAdapter;
        $this->slowDataSource = $slowDataSource;
    }

    /**
     * @param $id
     *
     * @return array|null
     *
     * @throws InvalidArgumentException
     */
    public function getWatchById($id): ?array
    {
        if (!is_numeric($id) || (is_numeric($id) && $id <= 0)) {
            throw new WatchIdNotValidException('Id needs to be a positive integer');
        }

        $cacheKey = self::CACHE_PREFIX . '_' . $id;
        $cachedData = $this->cacheAdapter->getItem($cacheKey);

        if (!$cachedData->isHit()) {

            if ($this->slowDataSource === SlowDataSourceEnum::XML) {
                $watch = $this->xmlWatchLoader->loadByIdFromXml($id);

                // Populating response array and explicit casting - there is discrepancy
                // in task document between int types in array that loadByIdFromXml returns
                // and since I wasn't sure if this was done on purpose or mistake, I've opted out
                // for following original response format
                $data = [
                    'identification' => (int)$watch['identification'],
                    'title' => $watch['title'],
                    'price' => (int)$watch['price'],
                    'description' => $watch['description'],
                ];
            } elseif ($this->slowDataSource === SlowDataSourceEnum::MYSQL) {
                $watch = $this->mySqlWatchRepository->getWatchById($id);

                // Populating response array, some custom mapper could be written here in future to improve this
                $data = [
                    'identification' => (int)$id,
                    'title' => $watch->title,
                    'price' => (int)$watch->price,
                    'description' => $watch->description,
                ];
            } else {
                throw new SlowDataSourceNotValidException('Slow data source set to invalid type');
            }

            $cachedData->set($data);
            $cachedData->expiresAfter(new DateInterval('P10Y')); // Cache for 10 years
            $this->cacheAdapter->save($cachedData);

            $data['data_source'] = DataSourceEnum::SLOW . '_' . $this->slowDataSource;
        } else {
            $data = $cachedData->get('value');
            $data['data_source'] = DataSourceEnum::FAST;
        }

        // I know data_source wasn't in original format, it's just something I've implemented for purpose,
        // that you want to preview and actually run this code and quickly see what's going on
        return $data;
    }
}
