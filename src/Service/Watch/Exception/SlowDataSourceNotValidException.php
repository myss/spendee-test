<?php

namespace App\Service\Watch\Exception;

use RuntimeException;

class SlowDataSourceNotValidException extends RuntimeException
{
}
