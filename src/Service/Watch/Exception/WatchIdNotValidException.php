<?php

namespace App\Service\Watch\Exception;

use RuntimeException;

class WatchIdNotValidException extends RuntimeException
{
}
