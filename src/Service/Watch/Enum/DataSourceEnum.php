<?php

namespace App\Service\Watch\Enum;

// Placed data source enums here for now, since they're only used by WatchService.
// In future, these kind of data sources are used for more things,
// it would make more sense to switch enums to top-level namespace
class DataSourceEnum
{
    public const FAST = 'fast';
    public const SLOW = 'slow';
}
