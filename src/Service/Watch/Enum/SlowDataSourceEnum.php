<?php

namespace App\Service\Watch\Enum;

class SlowDataSourceEnum
{
    public const MYSQL = 'mysql';
    public const XML = 'xml';
}
