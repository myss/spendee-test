<?php

namespace App\Repository;

use App\Dto\Mapper\MysqlWatchMapper;
use App\Dto\MySqlWatchDTO;
use App\Entity\Watch;
use App\Repository\Exception\MySqlWatchNotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query;

class MySqlWatchRepository extends ServiceEntityRepository implements MySqlWatchRepositoryInterface
{
    private $mysqlWatchMapper;

    public function __construct(ManagerRegistry $registry, MysqlWatchMapper $mysqlWatchMapper)
    {
        $this->mysqlWatchMapper = $mysqlWatchMapper;
        parent::__construct($registry, Watch::class);
    }

    /**
     * @param int $id
     *
     * @return MySqlWatchDTO
     *
     * @throws NonUniqueResultException
     */
    public function getWatchById(int $id): MySqlWatchDTO
    {
        // Note #1: So we actually have two methods named getWatchById, one in WatchService and
        // another here. I left it like that since I can't change it here, and for me it makes more sense in
        // WatchService. If I was allowed, I'd change it here to findWatchById, just to follow doctrine's best practices
        // Note #2: Using partial objects which only hydrate specified fields,
        // as performance might be of significance here
        $watch = $this->createQueryBuilder('w')
            ->select('partial w.{id, title, price, description}')
            ->where('w.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1)
            ->getOneOrNullResult();

        if ($watch === null) {
            throw new MySqlWatchNotFoundException('Watch with ID ' . $id . ' not found');
        }
        return $this->mysqlWatchMapper->mapToDto($watch);
    }
}
