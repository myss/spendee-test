<?php

namespace App\Repository\Exception;

use RuntimeException;

class MySqlRepositoryException extends RuntimeException
{
}
