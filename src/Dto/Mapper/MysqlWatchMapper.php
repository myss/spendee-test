<?php

namespace App\Dto\Mapper;

use App\Dto\MySqlWatchDTO;
use App\Entity\Watch;

// A bit un-flexible way, in case we have a lot more of those, I'd probably look into
// implementing something like AutoMapper: https://github.com/mark-gerarts/automapper-plus
// We could also here method mapToEntity, as in from DTO
// Maybe even mapToArray, but not sure about this one, would require some more knowledge about system architecture
class MysqlWatchMapper
{
    public function mapToDto(Watch $watch): MySqlWatchDTO
    {
        return new MySqlWatchDTO(
            $watch->getId(),
            $watch->getTitle(),
            $watch->getPrice(),
            $watch->getDescription()
        );
    }
}
