<?php

namespace App\Response;

use Symfony\Component\HttpFoundation\Response;

class JsonSuccess extends Response
{
    public function __construct($data = null, int $status = Response::HTTP_OK)
    {
        if (is_array($data)) {
            $data = json_encode($data);
        }
        parent::__construct(
            $data,
            $status,
            ['Content-Type' => 'application/json']
        );
    }
}
