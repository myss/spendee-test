<?php

namespace App\Response;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonError extends JsonResponse
{
    public function __construct(Exception $exception, int $status = Response::HTTP_INTERNAL_SERVER_ERROR)
    {
        // Note #1: For development purposes I've added line and file, we don't want it on production
        // Note #2: Ideally it would be to introduce one more type class, like JsonCriticalError,
        // and add there debug details for critical exceptions on development
        // Note #4: Also one thing that requires a bit more effort but could be useful is writing
        // custom listener to listen on kernel exceptions, so we don't have twig exceptions
        // Note #5: I really hate using global variables, it's just for this time I couldn't think any better way
        // and I needed to go further with code - didn't want to spend much time on investigating this for now

        if ($_ENV['APP_ENV'] === 'dev') {
            $error = sprintf(
                '%s, file: %s, line: %s',
                $exception->getMessage(),
                $exception->getFile(),
                $exception->getLine()
            );
        } else {
            $error = sprintf(
                '%s',
                $exception->getMessage()
            );
        }

        parent::__construct(
            ['error' => $error],
            $status,
            ['Content-Type' => 'application/json']
        );
    }
}