#!/bin/bash
set -e

php -d memory_limit=-1 $(which composer) install
php bin/console cache:warmup --env=$APP_ENV
php bin/console assets:install --env=$APP_ENV --symlink
chown -R www-data:www-data $(pwd)/var/cache && chown -R www-data:www-data $(pwd)/var/log

if [ -n $MYSQL_HOST ] && [ -n $MYSQL_PORT ]; then
  ./bin/wait-for-it.sh --host=$MYSQL_HOST --port=$MYSQL_PORT
  php bin/console doctrine:migrations:migrate --env=$APP_ENV --no-interaction
fi

exec "$@"
